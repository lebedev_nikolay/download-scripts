echo "Installing epel repo"
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
echo "Installing ansible,sshpass and git"
yum install wget ansible git sshpass -y
echo "Clone vesta cp repo"
git clone https://lebedev_nikolay@bitbucket.org/lebedev_nikolay/vesta-cp.git /etc/ansible/roles/vesta-cp
echo "Installing vesta cp"
ansible-playbook /etc/ansible/roles/vesta-cp/tasks/playbook-install-vesta-cp.yml