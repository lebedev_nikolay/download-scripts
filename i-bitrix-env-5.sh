echo "Installing epel repo"
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
echo "Installing ansible,sshpass and git"
yum install wget ansible git sshpass -y
echo "Clone bitrix-env repo"
git clone https://lebedev_nikolay@bitbucket.org/lebedev_nikolay/bitrix-env.git /etc/ansible/roles/bitrix-env
echo "Installing bitrix-env"
ansible-playbook /etc/ansible/roles/bitrix-env/tasks/playbook-install-bitrix-env-5.yml