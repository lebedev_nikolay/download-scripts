#install prometeus server + node explorer + alert manager+ graphana+ remote clients node -explorer
echo "Installing epel repo"
rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-6.noarch.rpm
echo "Installing ansible and git"
yum install ansible git sshpass -y
echo "Clone ansible-prometheus repo"
git clone https://lebedev_nikolay@bitbucket.org/lebedev_nikolay/ansible-prometheus.git /etc/ansible/roles/ansible-prometheus
echo "Installing prometeus+node+alert+grafana"
ansible-playbook /etc/ansible/roles/ansible-prometheus/tasks/playbook-prom-alert-node-grafa.yml
echo "Installing remote clients"
ansible-playbook /etc/ansible/roles/ansible-prometheus/tasks/playbook-install-remote-clients.yml